class PID:
    def __init__(self, target, sample_rate, min_value, max_value, kp=1, ki=1, kd=1):
        self.p_constant = kp
        self.i_constant = ki
        self.d_constant = kd
        self.target = target
        self.error_accumulator = 0
        self.last_value = 0
        self.sample_rate = sample_rate
        self.min_value = min_value
        self.max_value = max_value

    def set_P_constant(self, value):
        self.p_constant = value

    def set_I_constant(self, value):
        self.i_constant = value

    def set_D_constant(self, value):
        self.d_constant = value

    def update(self, sensor_value):
        error = self.target - sensor_value
        self.error_accumulator += error
        d = (sensor_value - self.last_value) / self.sample_rate
        signal = (
            self.p_constant * error
            + self.i_constant * self.error_accumulator
            + self.d_constant * d
        )
        print(self.error_accumulator)

        self.last_value = sensor_value

        if signal > self.max_value:
            signal = self.max_value
        elif signal < self.min_value:
            signal = self.min_value

        return signal
