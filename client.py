import network
import time
from umqtt.robust import MQTTClient
import os
import gc
import sys
import machine
from machine import Pin, ADC, PWM
from math import log
import TemperatureSensor
import DC_MOTOR
import RGB_SENSOR
import PID


class Client:
    # callback is for everything that the website controls for the physical system
    def __init__(
        self,
        aio_username: bytes,
        aio_key: bytes,
        ssid: str,
        ssid_pw: str,
        mqtt_client_id: bytes,
    ) -> None:
        self.AIO_USERNAME = aio_username
        self.AIO_KEY = aio_key
        self.client = None
        self.broker = b"io.adafruit.com"
        self.port = 1883
        self.client_id = mqtt_client_id
        self.SSID = ssid
        self.PW = ssid_pw

    def connect_to_wifi(self):
        # Turn off the WiFi Access Point
        ap_if = network.WLAN(network.AP_IF)
        ap_if.active(False)

        # Connect the device to the WiFi network
        wifi = network.WLAN(network.STA_IF)
        wifi.active(True)
        wifi.connect(self.SSID, self.PW)

        MAX_ATTEMPTS = 99
        attempt_count = 0

        while not wifi.isconnected() and attempt_count < MAX_ATTEMPTS:
            attempt_count += 1
            time.sleep(1)

        if attempt_count == MAX_ATTEMPTS:
            print("Could not connect to the WiFi network")
            sys.exit()
        print("Connected to WiFi")

    def connect(self):
        self.connect_to_wifi()

        self.client: MQTTClient = MQTTClient(
            client_id=self.client_id,
            server=self.broker,
            user=self.AIO_USERNAME,
            password=self.AIO_KEY,
            ssl=False,
        )

        try:
            self.client.connect()
            print("Connected to Adafruit IO")
        except Exception as e:
            print("Could not connect to MQTT server {}{}".format(type(e).__name__, e))
            sys.exit()

    def disconnect(self):
        if self.client:
            self.client.disconnect()
            print("Disconnected from Adafruit IO")

    def get_feedname(self, feedname: bytes):
        mqtt_feedname = bytes(
            "{:s}/feeds/{:s}".format(
                self.AIO_USERNAME.decode("utf-8"), feedname.decode("utf-8")
            ),
            "utf-8",
        )
        return mqtt_feedname

    def publish(self, feedname: bytes, message: str):
        if not self.client:
            print("Not connected to MQTT broker")
            return  # empty return
        feed_url = self.get_feedname(feedname)
        self.client.publish(feed_url, message)
        print(f"Published {message} to {feed_url}")

    # kinda unnecessary func cuz we can just while True: publish()
    def publish_periodic(self, message_func, feedname: bytes, publish_period_sec=10):
        accum_time = 0
        try:
            while True:
                if accum_time >= publish_period_sec:
                    message = message_func()
                    self.publish(feedname, message)
                    accum_time = 0
                time.sleep(0.5)
                accum_time += 0.5
        except KeyboardInterrupt:
            print("Ctrl-C pressed...exiting")
            self.disconnect()
            sys.exit()

    def subscribe(self, feed_names):
        if not self.client:
            print("Not connected to MQTT broker")
            return
        self.client.set_callback(self.on_message)
        for feed_name in feed_names:
            mqtt_feedname = self.get_feedname(feed_name)
            self.client.subscribe(mqtt_feedname)
            print(f"Subscribed to feed: {mqtt_feedname}")

    def subscribe_and_check(self, feedname: bytes, check_period_sec=0.5):
        self.subscribe(feedname)
        try:
            while True:
                self.client.check_msg()
                time.sleep(check_period_sec)
        except KeyboardInterrupt:
            print("Ctrl-C pressed...exiting")
            self.disconnect()
            sys.exit()

    def on_message(self, topic, msg):
        print(f"Message received: {msg} on topic: {topic}")
        if b"pid.proportional" in topic:
            self.handle_proportional(msg)
        elif b"pid.integral" in topic:
            self.handle_integral(msg)
        elif b"pid.derivative" in topic:
            self.handle_derivative(msg)
        elif b"led.control" in topic:
            self.handle_led(msg)

    def handle_proportional(self, msg: bytes) -> None:
        try:
            value = float(msg.decode("utf-8"))
            self.pid.set_Kp(value)
            print(f"Proportional value updated to: {value}")
        except ValueError:
            print(f"Invalid proportional value: {msg}")

    def handle_integral(self, msg: bytes) -> None:
        try:
            value = float(msg.decode("utf-8"))
            self.pid.set_Ki(value)
            print(f"Integral value updated to: {value}")
        except ValueError:
            print(f"Invalid integral value: {msg}")

    def handle_derivative(self, msg: bytes) -> None:
        try:
            value = float(msg.decode("utf-8"))
            self.pid.set_Kd(value)
            print(f"Derivative value updated to: {value}")
        except ValueError:
            print(f"Invalid derivative value: {msg}")

    def handle_temperature(self, msg: bytes) -> None:
        # handle temperature message
        print(f"Temperature value received: {msg}")

    def handle_led(self, msg: bytes) -> None:
        # toggle led on off
        print(f"LED control message received: {msg}")

    def check_messages(self):
        if not self.client:
            print("Not connected to MQTT broker")
            return
        self.client.check_msg()


# using class1
if __name__ == "__main__":
    aio_username = b"ajcs"
    aio_key = b"aio_tmLB64jFfELSf9wXUJNdCXsmZeve"
    ssid = "aa"
    ssid_pw = "12345678"
    mqtt_client_id = bytes(
        "client_" + str(int.from_bytes(os.urandom(3), "little")), "utf-8"
    )

    mqtt_client = Client(aio_username, aio_key, ssid, ssid_pw, mqtt_client_id)
    mqtt_client.connect()

    # init sensors
    temp_sensor = TemperatureSensor(32)
    absorbance_sensor = RGB_SENSOR()
    pid = PID(target=16, sample_rate=2.5, min_value=14, max_value=20, kp=1, ki=1, kd=1)

    pid_feeds = ["pid.proportional", "pid.integral", "pid.derivative"]
    mqtt_client.subscribe(pid_feeds)

    try:
        while True:
            mqtt_client.check_messages()
            temp = temp_sensor.read_temperature()
            pid.update(sensor_value=temp)
            rgb = absorbance_sensor.get_rgb(absorbance_sensor.sensor.read(True))
            absorbance = absorbance_sensor.absorbance(rgb)

            print(f"Temperature: {temp:.2f} C, Absorbance: {absorbance: .2f}")

            current_time = time.time()
            log_entry = f"{current_time:.2f}, {temp:.2f}, {absorbance: .2f}\n"

            with open("sensor_data_log.txt", "a") as log_file:
                log_file.write(log_entry)

            try:
                mqtt_client.publish(b"temperature", f"{temp:.2f}")
                mqtt_client.publish(b"absorbance", f"{absorbance}")
            except Exception as e:
                print(f"Failed to publish data: {str(e)}")

            time.sleep(
                10
            )  # Sleep time can be adjusted based on the desired data acquisition rate

            print("RGB: " + str(rgb))
    except KeyboardInterrupt:
        print("Aborted")
        sys.exit()

    finally:
        mqtt_client.disconnect()
