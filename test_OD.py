from tcs34725 import *
from machine import Pin, SoftI2C
from time import sleep
import ssd1306


# init sensor and i2c
# todo: add oled print
i2c = SoftI2C(scl=Pin(22), sda=Pin(23), freq=100000)
sensor = TCS34725(i2c)
oled = ssd1306.SSD1306_I2C(128, 64, i2c)
oled.fill(0)
sensor.integration_time = 600
sensor.gain = 1
print("im alive")

if __name__ == "__main__":
    while True:
        sleep(0.5)
        data = sensor.read(raw=True)
        print(data)
