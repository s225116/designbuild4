import network
import time
from umqtt.robust import MQTTClient
import os
import math
import gc
import machine
import si1145
import sys
from machine import Pin, ADC
from math import log


adc_V_lookup = [
    0.006176471,
    0.008235294,
    0.01647059,
    0.02470588,
    0.02779412,
    0.03088236,
    0.03397059,
    0.03705883,
    0.04117647,
    0.04529412,
    0.04941177,
    0.0525,
    0.05558824,
    0.05867648,
    0.06176471,
    0.06485295,
    0.06794118,
    0.07102942,
    0.07411765,
    0.0782353,
    0.08235294,
    0.08647059,
    0.08955883,
    0.09264707,
    0.0957353,
    0.09882354,
    0.1019118,
    0.105,
    0.1080882,
    0.1111765,
    0.1152941,
    0.1194118,
    0.1235294,
    0.1266177,
    0.1297059,
    0.1327941,
    0.1358824,
    0.1389706,
    0.1420588,
    0.1451471,
    0.1482353,
    0.1513235,
    0.1544118,
    0.1575,
    0.1605882,
    0.1636765,
    0.1667647,
    0.1698529,
    0.1729412,
    0.1760294,
    0.1791177,
    0.1822059,
    0.1852941,
    0.1914706,
    0.1976471,
    0.2001177,
    0.2025882,
    0.2050588,
    0.2075294,
    0.21,
    0.2141177,
    0.2182353,
    0.222353,
    0.2254412,
    0.2285294,
    0.2316177,
    0.2347059,
    0.2377941,
    0.2408824,
    0.2439706,
    0.2470588,
    0.2501471,
    0.2532353,
    0.2563236,
    0.2594118,
    0.2635294,
    0.2676471,
    0.2717647,
    0.274853,
    0.2779412,
    0.2810294,
    0.2841177,
    0.2882353,
    0.2923529,
    0.2964706,
    0.2995588,
    0.3026471,
    0.3057353,
    0.3088235,
    0.3119118,
    0.315,
    0.3180882,
    0.3211765,
    0.3242647,
    0.327353,
    0.3304412,
    0.3335294,
    0.3366177,
    0.3397059,
    0.3427941,
    0.3458824,
    0.3489706,
    0.3520588,
    0.3551471,
    0.3582353,
    0.362353,
    0.3664706,
    0.3705883,
    0.3736765,
    0.3767647,
    0.379853,
    0.3829412,
    0.3854118,
    0.3878823,
    0.390353,
    0.3928235,
    0.3952941,
    0.3994118,
    0.4035295,
    0.4076471,
    0.4101177,
    0.4125883,
    0.4150588,
    0.4175294,
    0.42,
    0.4241177,
    0.4282353,
    0.432353,
    0.4348236,
    0.4372941,
    0.4397647,
    0.4422353,
    0.4447059,
    0.4477942,
    0.4508824,
    0.4539706,
    0.4570589,
    0.4601471,
    0.4632353,
    0.4663236,
    0.4694118,
    0.4725,
    0.4755883,
    0.4786765,
    0.4817647,
    0.4858823,
    0.4900001,
    0.4941177,
    0.4982353,
    0.502353,
    0.5064706,
    0.5095589,
    0.5126471,
    0.5157353,
    0.5188236,
    0.5219118,
    0.525,
    0.5280883,
    0.5311765,
    0.5342648,
    0.537353,
    0.5404412,
    0.5435295,
    0.5466177,
    0.5497059,
    0.5527942,
    0.5558824,
    0.5589706,
    0.5620589,
    0.5651471,
    0.5682353,
    0.5723529,
    0.5764706,
    0.5805883,
    0.5847059,
    0.5888236,
    0.5929412,
    0.5960294,
    0.5991177,
    0.6022058,
    0.6052941,
    0.6094118,
    0.6135294,
    0.6176471,
    0.6201177,
    0.6225883,
    0.6250588,
    0.6275294,
    0.63,
    0.6330882,
    0.6361765,
    0.6392647,
    0.642353,
    0.6464706,
    0.6505883,
    0.6547059,
    0.6577941,
    0.6608824,
    0.6639706,
    0.6670588,
    0.6695294,
    0.672,
    0.6744706,
    0.6769412,
    0.6794118,
    0.6855883,
    0.6917647,
    0.6942353,
    0.6967059,
    0.6991765,
    0.701647,
    0.7041177,
    0.7082353,
    0.712353,
    0.7164706,
    0.7195588,
    0.7226471,
    0.7257353,
    0.7288236,
    0.7329412,
    0.7370589,
    0.7411765,
    0.7436471,
    0.7461177,
    0.7485883,
    0.7510588,
    0.7535295,
    0.757647,
    0.7617648,
    0.7658824,
    0.768353,
    0.7708236,
    0.7732942,
    0.7757647,
    0.7782353,
    0.7844118,
    0.7905883,
    0.7936765,
    0.7967648,
    0.7998529,
    0.8029412,
    0.8060294,
    0.8091177,
    0.8122059,
    0.8152942,
    0.8183824,
    0.8214706,
    0.8245588,
    0.8276471,
    0.8301176,
    0.8325883,
    0.8350588,
    0.8375295,
    0.8400001,
    0.8430882,
    0.8461765,
    0.8492647,
    0.852353,
    0.8554412,
    0.8585295,
    0.8616177,
    0.8647059,
    0.8677941,
    0.8708824,
    0.8739706,
    0.8770589,
    0.8811766,
    0.8852942,
    0.8894118,
    0.8925,
    0.8955883,
    0.8986765,
    0.9017648,
    0.904853,
    0.9079412,
    0.9110294,
    0.9141177,
    0.9172059,
    0.9202942,
    0.9233824,
    0.9264707,
    0.9295588,
    0.9326471,
    0.9357353,
    0.9388236,
    0.9429413,
    0.9470588,
    0.9511765,
    0.9542647,
    0.957353,
    0.9604412,
    0.9635295,
    0.9666177,
    0.969706,
    0.9727942,
    0.9758824,
    0.9789706,
    0.9820589,
    0.9851471,
    0.9882354,
    0.9923531,
    0.9964705,
    1.000588,
    1.003677,
    1.006765,
    1.009853,
    1.012941,
    1.016029,
    1.019118,
    1.022206,
    1.025294,
    1.028382,
    1.031471,
    1.034559,
    1.037647,
    1.040735,
    1.043824,
    1.046912,
    1.05,
    1.053088,
    1.056177,
    1.059265,
    1.062353,
    1.065441,
    1.068529,
    1.071618,
    1.074706,
    1.078824,
    1.082941,
    1.087059,
    1.090147,
    1.093235,
    1.096324,
    1.099412,
    1.1025,
    1.105588,
    1.108677,
    1.111765,
    1.114853,
    1.117941,
    1.121029,
    1.124118,
    1.127206,
    1.130294,
    1.133382,
    1.136471,
    1.139559,
    1.142647,
    1.145735,
    1.148824,
    1.152941,
    1.157059,
    1.161177,
    1.164265,
    1.167353,
    1.170441,
    1.17353,
    1.176,
    1.178471,
    1.180941,
    1.183412,
    1.185882,
    1.188971,
    1.192059,
    1.195147,
    1.198235,
    1.201324,
    1.204412,
    1.2075,
    1.210588,
    1.214706,
    1.218824,
    1.222941,
    1.226029,
    1.229118,
    1.232206,
    1.235294,
    1.237765,
    1.240235,
    1.242706,
    1.245177,
    1.247647,
    1.253824,
    1.26,
    1.262059,
    1.264118,
    1.266176,
    1.268235,
    1.270294,
    1.272353,
    1.275441,
    1.278529,
    1.281618,
    1.284706,
    1.288824,
    1.292941,
    1.297059,
    1.300147,
    1.303235,
    1.306324,
    1.309412,
    1.3125,
    1.315588,
    1.318676,
    1.321765,
    1.325882,
    1.33,
    1.334118,
    1.336588,
    1.339059,
    1.341529,
    1.344,
    1.346471,
    1.350588,
    1.354706,
    1.358824,
    1.361912,
    1.365,
    1.368088,
    1.371176,
    1.374265,
    1.377353,
    1.380441,
    1.383529,
    1.387647,
    1.391765,
    1.395882,
    1.398353,
    1.400824,
    1.403294,
    1.405765,
    1.408235,
    1.412353,
    1.416471,
    1.420588,
    1.423676,
    1.426765,
    1.429853,
    1.432941,
    1.435412,
    1.437882,
    1.440353,
    1.442824,
    1.445294,
    1.449412,
    1.453529,
    1.457647,
    1.460735,
    1.463824,
    1.466912,
    1.47,
    1.474118,
    1.478235,
    1.482353,
    1.485441,
    1.488529,
    1.491618,
    1.494706,
    1.497794,
    1.500882,
    1.503971,
    1.507059,
    1.510147,
    1.513235,
    1.516324,
    1.519412,
    1.52353,
    1.527647,
    1.531765,
    1.548235,
    1.564706,
    1.581177,
    1.562647,
    1.544118,
    1.548235,
    1.552353,
    1.575,
    1.559559,
    1.562647,
    1.565735,
    1.587353,
    1.585294,
    1.601765,
    1.618235,
    1.620706,
    1.623176,
    1.625647,
    1.628118,
    1.630588,
    1.633677,
    1.636765,
    1.639853,
    1.642941,
    1.646029,
    1.649118,
    1.652206,
    1.655294,
    1.659412,
    1.663529,
    1.667647,
    1.671765,
    1.675882,
    1.68,
    1.682471,
    1.684941,
    1.687412,
    1.689882,
    1.692353,
    1.696471,
    1.700588,
    1.704706,
    1.706765,
    1.708824,
    1.710882,
    1.712941,
    1.715,
    1.717059,
    1.720147,
    1.723235,
    1.726324,
    1.729412,
    1.7325,
    1.735588,
    1.738677,
    1.741765,
    1.745882,
    1.75,
    1.754118,
    1.758235,
    1.762353,
    1.766471,
    1.770588,
    1.774706,
    1.778824,
    1.781912,
    1.785,
    1.788088,
    1.791177,
    1.793647,
    1.796118,
    1.798588,
    1.801059,
    1.80353,
    1.807647,
    1.811765,
    1.815882,
    1.82,
    1.824118,
    1.828235,
    1.830706,
    1.833177,
    1.835647,
    1.838118,
    1.840588,
    1.843677,
    1.846765,
    1.849853,
    1.852941,
    1.85603,
    1.859118,
    1.862206,
    1.865294,
    1.869412,
    1.87353,
    1.877647,
    1.880735,
    1.883824,
    1.886912,
    1.89,
    1.893088,
    1.896177,
    1.899265,
    1.902353,
    1.905441,
    1.908529,
    1.911618,
    1.914706,
    1.917794,
    1.920882,
    1.923971,
    1.927059,
    1.931176,
    1.935294,
    1.939412,
    1.943529,
    1.947647,
    1.951765,
    1.954853,
    1.957941,
    1.96103,
    1.964118,
    1.967206,
    1.970294,
    1.973382,
    1.976471,
    1.978941,
    1.981412,
    1.983883,
    1.986353,
    1.988824,
    1.991912,
    1.995,
    1.998088,
    2.001177,
    2.004265,
    2.007353,
    2.010441,
    2.01353,
    2.019706,
    2.025882,
    2.028353,
    2.030823,
    2.033294,
    2.035765,
    2.038235,
    2.041324,
    2.044412,
    2.0475,
    2.050588,
    2.053677,
    2.056765,
    2.059853,
    2.062941,
    2.06603,
    2.069118,
    2.072206,
    2.075294,
    2.079412,
    2.083529,
    2.087647,
    2.090735,
    2.093824,
    2.096912,
    2.1,
    2.106177,
    2.112353,
    2.115441,
    2.11853,
    2.121618,
    2.124706,
    2.126765,
    2.128824,
    2.130883,
    2.132941,
    2.135,
    2.137059,
    2.139529,
    2.142,
    2.144471,
    2.146941,
    2.149412,
    2.1525,
    2.155588,
    2.158677,
    2.161765,
    2.167941,
    2.174118,
    2.178235,
    2.182353,
    2.186471,
    2.189559,
    2.192647,
    2.195735,
    2.198824,
    2.201294,
    2.203765,
    2.206235,
    2.208706,
    2.211177,
    2.214265,
    2.217353,
    2.220441,
    2.22353,
    2.227647,
    2.231765,
    2.235883,
    2.238971,
    2.242059,
    2.245147,
    2.248235,
    2.251324,
    2.254412,
    2.2575,
    2.260588,
    2.263677,
    2.266765,
    2.269853,
    2.272941,
    2.27603,
    2.279118,
    2.282206,
    2.285294,
    2.289412,
    2.29353,
    2.297647,
    2.300118,
    2.302588,
    2.305059,
    2.307529,
    2.31,
    2.313088,
    2.316177,
    2.319265,
    2.322353,
    2.326471,
    2.330588,
    2.334706,
    2.337177,
    2.339647,
    2.342118,
    2.344588,
    2.347059,
    2.350147,
    2.353235,
    2.356324,
    2.359412,
    2.363529,
    2.367647,
    2.371765,
    2.375882,
    2.38,
    2.384118,
    2.388235,
    2.392353,
    2.396471,
    2.39853,
    2.400588,
    2.402647,
    2.404706,
    2.406765,
    2.408823,
    2.411912,
    2.415,
    2.418088,
    2.421176,
    2.424265,
    2.427353,
    2.430441,
    2.433529,
    2.436618,
    2.439706,
    2.442794,
    2.445882,
    2.448971,
    2.452059,
    2.455147,
    2.458235,
    2.462353,
    2.466471,
    2.470588,
    2.473059,
    2.475529,
    2.478,
    2.480471,
    2.482941,
    2.486029,
    2.489118,
    2.492206,
    2.495294,
    2.497765,
    2.500235,
    2.502706,
    2.505177,
    2.507647,
    2.511765,
    2.515882,
    2.52,
    2.523088,
    2.526176,
    2.529265,
    2.532353,
    2.534412,
    2.536471,
    2.538529,
    2.540588,
    2.542647,
    2.544706,
    2.547794,
    2.550882,
    2.553971,
    2.557059,
    2.561177,
    2.565294,
    2.569412,
    2.5725,
    2.575588,
    2.578676,
    2.581765,
    2.584853,
    2.587941,
    2.591029,
    2.594118,
    2.597206,
    2.600294,
    2.603382,
    2.606471,
    2.608941,
    2.611412,
    2.613883,
    2.616353,
    2.618824,
    2.621912,
    2.625,
    2.628088,
    2.631176,
    2.633235,
    2.635294,
    2.637353,
    2.639412,
    2.641471,
    2.643529,
    2.647647,
    2.651765,
    2.655882,
    2.658971,
    2.662059,
    2.665147,
    2.668235,
    2.670294,
    2.672353,
    2.674412,
    2.676471,
    2.67853,
    2.680588,
    2.684706,
    2.688824,
    2.692941,
    2.695,
    2.697059,
    2.699118,
    2.701177,
    2.703235,
    2.705294,
    2.708382,
    2.711471,
    2.714559,
    2.717647,
    2.719706,
    2.721765,
    2.723824,
    2.725883,
    2.727941,
    2.73,
    2.733088,
    2.736176,
    2.739265,
    2.742353,
    2.744824,
    2.747294,
    2.749765,
    2.752235,
    2.754706,
    2.757176,
    2.759647,
    2.762118,
    2.764588,
    2.767059,
    2.769529,
    2.772,
    2.774471,
    2.776941,
    2.779412,
    2.781882,
    2.784353,
    2.786824,
    2.789294,
    2.791765,
    2.794235,
    2.796706,
    2.799177,
    2.801647,
    2.804118,
    2.805882,
    2.807647,
    2.809412,
    2.811176,
    2.812941,
    2.814706,
    2.816471,
    2.820588,
    2.824706,
    2.828824,
    2.830883,
    2.832941,
    2.835,
    2.837059,
    2.839118,
    2.841177,
    2.843235,
    2.845294,
    2.847353,
    2.849412,
    2.851471,
    2.853529,
    2.855294,
    2.857059,
    2.858824,
    2.860588,
    2.862353,
    2.864118,
    2.865882,
    2.867941,
    2.87,
    2.872059,
    2.874118,
    2.876177,
    2.878235,
    2.880294,
    2.882353,
    2.884412,
    2.886471,
    2.88853,
    2.890588,
    2.893059,
    2.89553,
    2.898,
    2.900471,
    2.902941,
    2.905,
    2.907059,
    2.909118,
    2.911177,
    2.913235,
    2.915294,
    2.917353,
    2.919412,
    2.921471,
    2.92353,
    2.925588,
    2.927647,
    2.929412,
    2.931177,
    2.932941,
    2.934706,
    2.936471,
    2.938235,
    2.94,
    2.942059,
    2.944118,
    2.946177,
    2.948236,
    2.950294,
    2.952353,
    2.954823,
    2.957294,
    2.959765,
    2.962235,
    2.964706,
    2.966765,
    2.968824,
    2.970882,
    2.972941,
    2.975,
    2.977059,
    2.979118,
    2.981177,
    2.983235,
    2.985294,
    2.987353,
    2.989412,
    2.991471,
    2.99353,
    2.995588,
    2.997647,
    2.999706,
    3.001765,
    3.00353,
    3.005294,
    3.007059,
    3.008824,
    3.010588,
    3.012353,
    3.014118,
    3.015882,
    3.017647,
    3.019412,
    3.021177,
    3.022941,
    3.024706,
    3.026471,
    3.028015,
    3.029559,
    3.031103,
    3.032647,
    3.034191,
    3.035735,
    3.03728,
    3.038824,
    3.040883,
    3.042941,
    3.045,
    3.047059,
    3.049118,
    3.051177,
    3.052941,
    3.054706,
    3.056471,
    3.058235,
    3.06,
    3.061765,
    3.063529,
    3.065294,
    3.067059,
    3.068824,
    3.070588,
    3.072353,
    3.074118,
    3.075882,
    3.077941,
    3.08,
    3.082059,
    3.084118,
    3.086176,
    3.088235,
    3.08978,
    3.091324,
    3.092868,
    3.094412,
    3.095956,
    3.0975,
    3.099044,
    3.100588,
    3.106765,
    3.131471,
]
NOM_RES = 10000
SER_RES = 9820
TEMP_NOM = 25
NUM_SAMPLES = 25
THERM_B_COEFF = 3950
ADC_MAX = 1023
ADC_Vmax = 3.15

import machine
import math
import si1145


class SI1145Sensor:
    def __init__(self, sda_pin=23, scl_pin=22, retries=3):
        self.i2c = None
        self.sensor = None
        self.init_sensor(sda_pin, scl_pin, retries)
        self.incident_light = None

    def init_sensor(self, sda_pin, scl_pin, retries):
        while retries > 0:
            try:
                # Initialize I2C
                self.i2c = machine.I2C(
                    sda=machine.Pin(sda_pin), scl=machine.Pin(scl_pin)
                )
                # Initialize the SI1145 sensor
                self.sensor = si1145.SI1145(i2c=self.i2c)
                return  # Exit after successful initialization
            except Exception as e:
                print(f"Failed to initialize SI1145 sensor: {str(e)}")
                retries -= 1
                time.sleep(1)
        raise Exception("SI1145 sensor initialization failed after several attempts")

    def set_incident_light(self, intensity):
        """Set the incident light intensity manually."""
        self.incident_light = intensity

    def read_uv(self):
        """Read UV index from the sensor. Treat this as a property."""
        return self.sensor.read_uv

    def calculate_absorbance(self):
        if self.incident_light is None:
            raise ValueError(
                "Incident light intensity must be set before calculating absorbance."
            )
        transmitted_light = self.read_uv()
        if transmitted_light <= 0:
            raise ValueError(
                "Transmitted light must be greater than 0 to calculate absorbance."
            )
        transmittance = transmitted_light / self.incident_light
        absorbance = -math.log10(transmittance)
        return absorbance


class DC_MOTOR:
    # motor_1 enable = 26 input1a = 15 input2b = 33
    # motor_2 enable = 25 input1c = 12 input2d = 25
    # motor_1 = DC_MOTOR(15, 33, 26)
    # motor_2 = DC_MOTOR(27, 12, 25)
    frequency = 1000
    currentDutyCycle = 0

    maxCycles = 1023
    minCycles = 123  # adjustable min max

    def __init__(self, inputD, inputC, EnableB) -> None:
        self.inputD = machine.Pin(inputD, machine.Pin.OUT)
        self.inputC = machine.Pin(inputC, machine.Pin.OUT)
        self.EnableB = machine.Pin(EnableB, machine.Pin.OUT)
        self.pwmB = machine.PWM(self.EnableB)

        self.pwmB.freq(self.frequency)

    def start(self):
        self.inputD.value(1)
        self.inputC.value(0)

    def stop(self):
        self.inputD.value(0)
        self.inputC.value(0)

    def setSpeedCycles(self, dutyCycles):
        self.currentDutyCycle = dutyCycles
        self.pwmB.duty(dutyCycles)

    def setSpeedPercentage(self, percentage):
        self.currentDutyCycle = self.percentageToDutyCycle(percentage)
        self.pwmB.duty(int(self.currentDutyCycle))

    def getSpeedPercentage(self):
        percentage = self.dutyCycleToPercentage(self.currentDutyCycle)
        return percentage

    def dutyCycleToPercentage(self, dutyCycle):
        percentage = 100 * (
            (dutyCycle - self.minCycles) / (self.maxCycles - self.minCycles)
        )
        return percentage

    def percentageToDutyCycle(self, percentage):
        dutyCycle = (
            (self.maxCycles - self.minCycles) * percentage / 100
        ) + self.minCycles
        return dutyCycle

    def testMinAndMaxDuty(self):
        print("running large DC testMinAndMaxDuty....")

        self.start()
        duty = 0
        while duty < self.maxCycles:
            self.setSpeedCycles(duty)
            time.sleep(0.01)
            duty += 1

        print("Test is done")
        time.sleep(3)
        self.stop()

    def testMaxSpeed(self):
        print("running large DC testMaxSpeed....")
        self.start()
        iter = 0
        while iter < 1000:
            print("running at max speed")
            self.setSpeedCycles(self.maxCycles)
            time.sleep(0.01)
            iter += 1

        print("Test is done")
        self.stop()


class TemperatureSensor:
    def __init__(self, TEMP_SENS_ADC_PIN_NO=32):
        self.adc = self.init_temp_sensor(TEMP_SENS_ADC_PIN_NO)

    def init_temp_sensor(self, TEMP_SENS_ADC_PIN_NO):
        adc = ADC(Pin(TEMP_SENS_ADC_PIN_NO))
        adc.atten(ADC.ATTN_11DB)
        adc.width(ADC.WIDTH_10BIT)
        return adc

    def read_temperature(self):
        raw_read = []
        # Collect NUM_SAMPLES
        for i in range(NUM_SAMPLES):
            raw_read.append(self.adc.read())

        # Average of the NUM_SAMPLES and look it up in the table
        raw_average = sum(raw_read) / NUM_SAMPLES
        print("raw_avg = " + str(raw_average))
        adc_voltage = adc_V_lookup[round(raw_average)]
        print("V_measured = " + str(adc_voltage))

        # Convert to resistance
        raw_average = ADC_MAX * adc_voltage / ADC_Vmax
        resistance = (SER_RES * raw_average) / (ADC_MAX - raw_average)
        print("Thermistor resistance: {} ohms".format(resistance))

        # Convert to temperature
        steinhart = log(resistance / NOM_RES) / THERM_B_COEFF
        steinhart += 1.0 / (TEMP_NOM + 273.15)
        steinhart = (1.0 / steinhart) - 273.15
        return steinhart


class WiFiConnection:
    def __init__(self, ssid, password):
        self.ssid = ssid
        self.password = password
        self.wifi = network.WLAN(network.STA_IF)

    def connect(self):
        self.wifi.active(True)
        self.wifi.connect(self.ssid, self.password)

        max_attempts = 99
        for attempt_count in range(max_attempts):
            if self.wifi.isconnected():
                print("Connected to WiFi")
                return True
            print(f"Attempting to connect to WiFi: {attempt_count + 1}")
            time.sleep(1)

        print("Could not connect to the WiFi network")
        return False

    def is_connected(self):
        return self.wifi.isconnected()


class MQTTConnection:
    def __init__(self, client_id, server, username, password):
        self.client_id = client_id
        self.server = server
        self.username = username
        self.password = password
        self.client = MQTTClient(
            client_id=client_id,
            server=server,
            user=username,
            password=password,
            ssl=False,
        )
        self.proportional = 1
        self.integral = 1
        self.derivative = 1

    def connect(self):
        try:
            self.client.connect()
            print("Connected to MQTT broker")
        except Exception as e:
            print(f"Could not connect to MQTT server: {type(e).__name__} {e}")
            return False
        return True

    def reconnect(self):
        self.client = MQTTClient(
            client_id=self.client_id,
            server=self.server,
            user=self.username,
            password=self.password,
            ssl=False,
        )
        return self.connect()

    def publish(self, feed_name, msg):
        try:
            mqtt_feedname = f"{self.username}/feeds/{feed_name}"
            self.client.publish(mqtt_feedname, str(msg), qos=0)
            print(f"Published data: {msg}")
        except Exception as e:
            print(f"Error publishing to MQTT: {type(e).__name__} {e}")
            return False
        return True

    def get_feedname(self, feed_name):
        mqtt_feedname = f"{self.username}/feeds/{feed_name}"
        return mqtt_feedname

    def check_msg(self):
        self.client.check_msg()

    def subscribe(self, feed_names):
        self.client.set_callback(self.message_callback)
        for feed_name in feed_names:
            mqtt_feedname = self.get_feedname(feed_name)
            self.client.subscribe(mqtt_feedname)
            print(f"Subscribed to feed: {mqtt_feedname}")

    def message_callback(self, topic, msg):
        print(f"Received message from {topic}: {msg}")
        if "proportional" in topic.decode():
            self.proportional = float(msg)
        elif "integral" in topic.decode():
            self.integral = float(msg)
        elif "derivative" in topic.decode():
            self.derivative = float(msg)
        print(f"P: {self.proportional}, I: {self.integral}, D: {self.derivative}")

    def request_initial_values(self):
        # Publish requests to get the latest values
        self.publish("design4/request_proportional", "get_latest")
        self.publish("design4/request_integral", "get_latest")
        self.publish("design4/request_derivative", "get_latest")


def main():
    # Constants and configurations
    adafruit_io_url = "io.adafruit.com"
    adafruit_username = "Shierfall"
    adafruit_io_key = "aio_PgJe01AZdSkmIRsSoquiF05CKxY7"

    # Create a random MQTT clientID
    random_num = int.from_bytes(os.urandom(3), "little")
    mqtt_client_id = f"client_{random_num}"

    # Initialize connections
    wifi_connection = WiFiConnection("Cat", "internet1234")

    mqtt_connection = MQTTConnection(
        mqtt_client_id, adafruit_io_url, adafruit_username, adafruit_io_key
    )

    # Connect to WiFi and MQTT broker
    if not wifi_connection.connect():
        print("Failed to connect to WiFi after several attempts.")
        sys.exit()

    if not mqtt_connection.connect():
        print("Failed to establish a connection with the MQTT broker.")
        sys.exit()

    # Initialize sensors
    temp_sensor = TemperatureSensor(32)
    absorbance_sensor = SI1145Sensor(sda_pin=23, scl_pin=22)
    absorbance_sensor.set_incident_light(
        1000
    )  # Adjust this value based on your calibration data

    # Subscribe to PID feeds
    pid_feeds = ["design4.proportional", "design4.integral", "design4.derivative"]
    mqtt_connection.subscribe(pid_feeds)

    # Request initial PID values
    mqtt_connection.request_initial_values()

    # Main loop
    while True:
        # Check for new MQTT messages which may contain PID updates
        mqtt_connection.check_msg()

        # Read and process sensor data
        temp = temp_sensor.read_temperature()
        try:
            uv = absorbance_sensor.read_uv()
            absorbance = absorbance_sensor.calculate_absorbance()
        except Exception as e:
            absorbance = "N/A"  # Handle error in absorbance measurement

        print(
            f'Temperature: {temp:.2f} C, Absorbance: {absorbance if absorbance != "N/A" else absorbance}'
        )
        print("uv: " + str(uv))

        # Log and potentially publish data
        current_time = time.time()
        log_entry = f"{current_time:.2f}, {temp:.2f}, {absorbance}\n"

        with open("sensor_data_log.txt", "a") as log_file:
            log_file.write(log_entry)

        if wifi_connection.is_connected():
            try:
                mqtt_connection.publish("temperature", f"{temp:.2f}")
                if absorbance != "N/A":
                    mqtt_connection.publish("absorbance", f"{absorbance}")
            except Exception as e:
                print(f"Failed to publish data: {str(e)}")

        time.sleep(
            10
        )  # Sleep time can be adjusted based on the desired data acquisition rate


if __name__ == "__main__":
    main()
