from math import log
import tcs34725
from machine import I2C, Pin

class Calibrator:
    def __init__(self, scl_val = 22, sda_val = 23):
        self.lov = []
        self.sensor = tcs34725.TCS34725(I2C(scl=Pin(scl_val), sda=Pin(sda_val), freq=100000))
        self.I0 = -1


    #Takes I0 and appends to the list of values as (0, reading)
    def take_fixed_m (self, conc, print_val = True):
        #sensor.read returns a 4-tuple (R, G, B, Lx)
        self.I0 = self.get_rgb(self.sensor.read(False))       
        self.lov = self.lov + (conc, self.absorbance(self.I0)) 
        if print_val:
            print(self.lov)

    def take_m (self, conc, print_val = True):
        temp = self.get_rgb(self.sensor.read(False))
        self.lov = self.lov + (conc, self.absorbance(temp)) 
        if print_val:
            print(self.lov)

    def get_rgb(color_raw):

        r, g, b, clear = color_raw
        # Avoid divide by zero errors ... if clear = 0 return black
        if clear == 0:
            return (0, 0, 0)
        red   = (pow((((r/clear) * 256) / 255), 2.5) * 255)
        green = (pow((((g/clear) * 256) / 255), 2.5) * 255)
        blue  = (pow((((b/clear) * 256) / 255), 2.5) * 255)
        # Handle possible 8-bit overflow
        if red > 255:
            red = 255
        if green > 255:
            green = 255
        if blue > 255:
            blue = 255
        return (red, green, blue)

    def absorbance(self, I_val):
        if self.I0 == -1 :
            print("Fixed value I0 has not been taken, please take the fixed point reading.") 
        else:
            #Out of the RGB 4-tuple, takes the B value and computes log
            temp = -1 * log(I_val[2] / self.I0[2], 10) 
            return temp

    def get_array(self):
        print(self.lov)


calib = Calibrator() 
calib.get_array()
