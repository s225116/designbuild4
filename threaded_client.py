import network
import time
from umqtt.robust import MQTTClient
import os
import gc
import sys
import threading


class TClient:
    # callback is for everything that the website controls for the physical system
    def __init__(
        self,
        aio_username: bytes,
        aio_key: bytes,
        ssid: str,
        ssid_pw: str,
        mqtt_client_id: bytes,
    ) -> None:
        self.AIO_USERNAME = aio_username
        self.AIO_KEY = aio_key
        self.client = None
        self.broker = b"io.adafruit.com"
        self.port = 1883
        self.client_id = mqtt_client_id
        self.SSID = ssid
        self.PW = ssid_pw

    def connect_to_wifi(self):
        # Turn off the WiFi Access Point
        ap_if = network.WLAN(network.AP_IF)
        ap_if.active(False)

        # Connect the device to the WiFi network
        wifi = network.WLAN(network.STA_IF)
        wifi.active(True)
        wifi.connect(self.SSID, self.PW)

        MAX_ATTEMPTS = 20
        attempt_count = 0

        while not wifi.isconnected() and attempt_count < MAX_ATTEMPTS:
            attempt_count += 1
            time.sleep(1)

        if attempt_count == MAX_ATTEMPTS:
            print("Could not connect to the WiFi network")
            sys.exit()
        print("Connected to WiFi")

    def connect(self):
        self.connect_to_wifi()

        self.client: MQTTClient = MQTTClient(
            client_id=self.client_id,
            server=self.broker,
            user=self.AIO_USERNAME,
            password=self.AIO_KEY,
            ssl=False,
        )

        try:
            self.client.connect()
            print("Connected to Adafruit IO")
        except Exception as e:
            print("Could not connect to MQTT server {}{}".format(type(e).__name__, e))
            sys.exit()

    def disconnect(self):
        if self.client:
            self.client.disconnect()
            print("Disconnected from Adafruit IO")

    def get_feedname(self, feedname: bytes):
        mqtt_feedname = bytes(
            "{:s}/feeds/{:s}".format(
                self.AIO_USERNAME.decode("utf-8"), feedname.decode("utf-8")
            ),
            "utf-8",
        )
        return mqtt_feedname

    def publish(self, feedname: bytes, message: str):
        if not self.client:
            print("Not connected to MQTT broker")
            return  # empty return
        feed_url = self.get_feedname(feedname)
        self.client.publish(feed_url, message)
        print(f"Published {message} to {feed_url}")

    def publish_periodic(self, message_func, feedname: bytes, publish_period_sec=10):
        accum_time = 0
        try:
            while True:
                if accum_time >= publish_period_sec:
                    message = message_func()
                    self.publish(feedname, message)
                    accum_time = 0
                time.sleep(0.5)
                accum_time += 0.5
        except KeyboardInterrupt:
            print("Ctrl-C pressed...exiting")
            self.disconnect()
            sys.exit()

    def subscribe(self, feedname: bytes):
        if not self.client:
            print("Not connected to MQTT broker")
            return
        self.client.set_callback(self.on_message)
        feed_url = self.get_feedname(feedname)
        self.client.subscribe(feed_url)
        print(f"Subscribed to {feed_url}")

    def subscribe_and_check(self, feedname: bytes, check_period_sec=0.5):
        self.subscribe(feedname)
        try:
            while True:
                self.client.check_msg()
                time.sleep(check_period_sec)
        except KeyboardInterrupt:
            print("Ctrl-C pressed...exiting")
            self.disconnect()
            sys.exit()

    def on_message(self, topic, msg):
        print(f"Message received: {msg} on topic: {topic}")
        if b"pid.proportional" in topic:
            self.handle_proportional(msg)
        elif b"pid.integral" in topic:
            self.handle_integral(msg)
        elif b"pid.derivative" in topic:
            self.handle_derivative(msg)
        elif b"led.control" in topic:
            self.handle_led(msg)
        elif b"temperature" in topic:
            self.handle_temperature(msg)

    def handle_proportional(self, msg):
        print(f"Proportional value received: {msg}")

    def handle_integral(self, msg):
        print(f"Integral value received: {msg}")

    def handle_derivative(self, msg):
        print(f"Derivative value received: {msg}")

    def handle_led(self, msg):
        print(f"LED control message received: {msg}")

    def handle_temperature(self, msg):
        print(f"Temperature value received: {msg}")

    def check_messages(self):
        if not self.client:
            print("Not connected to MQTT broker")
            return
        self.client.check_msg()


# usage with threading
if __name__ == "__main__":
    aio_username = b"your_aio_username"
    aio_key = b"your_aio_key"
    ssid = "your_wifi_ssid"
    ssid_pw = "your_wifi_password"
    mqtt_client_id = bytes(
        "client_" + str(int.from_bytes(os.urandom(3), "little")), "utf-8"
    )

    mqtt_client = TClient(aio_username, aio_key, ssid, ssid_pw, mqtt_client_id)
    mqtt_client.connect()

    def get_free_heap():
        return str(gc.mem_free())

    # test thread for publishing periodic messages
    # might have to replace this with a while loop that just checks for stuff
    def publish_thread():
        mqtt_client.publish_periodic(
            get_free_heap, feedname=b"pid.proportional", publish_period_sec=5
        )

    # thread for subscribing and checking messages
    def subscribe_thread():
        mqtt_client.subscribe_and_check(feedname=b"temperature", check_period_sec=0.5)

    # create and start threads
    t1 = threading.Thread(target=publish_thread)
    t2 = threading.Thread(target=subscribe_thread)

    t1.start()
    t2.start()

    try:
        t1.join()
        t2.join()
    except KeyboardInterrupt:
        print("Interrupted")
    finally:
        mqtt_client.disconnect()
