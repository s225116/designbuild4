from machine import Pin, I2C
import designbuild4.tcs34725


class RGBSensor:
    def __init__(self) -> None:
        self.i2c: I2C = I2C(scl=Pin(22), sda=Pin(23), freq=400000)
        self.sensor = designbuild4.tcs34725.TCS34725(self.i2c)

    def set_gain(self, value: int):
        self.sensor.gain(value)

    def color_rgb_bytes(self, color_raw):
        r, g, b, clear = color_raw
        # Avoid divide by zero errors ... if clear = 0 return black
        if clear == 0:
            return (0, 0, 0)
        red = pow((((r / clear) * 256) / 255), 2.5) * 255
        green = pow((((g / clear) * 256) / 255), 2.5) * 255
        blue = pow((((b / clear) * 256) / 255), 2.5) * 255
        # Handle possible 8-bit overflow
        if red > 255:
            red = 255
        if green > 255:
            green = 255
        if blue > 255:
            blue = 255
        return (red, green, blue)
