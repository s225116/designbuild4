import network
import time
from umqtt.robust import MQTTClient
import os
import gc
import sys
import PID
import requests
import json
import threading
import ssd1306
import tcs34725
import math
from machine import I2C, Pin, ADC


class ColorSensor:
    def __init__(
        self, sda_pin=23, scl_pin=22, oled_width=128, oled_height=64, i2c_freq=100000
    ):
        self.i2c = I2C(scl=Pin(scl_pin), sda=Pin(sda_pin), freq=i2c_freq)
        self.oled = ssd1306.SSD1306_I2C(oled_width, oled_height, self.i2c)
        self.oled.fill(0)
        self.sensor = tcs34725.TCS34725(self.i2c)
        self.incident_light = None

    def set_incident_light(self, intensity):
        self.incident_light = intensity

    def color_rgb_bytes(self, color_raw):
        r, g, b, clear = color_raw
        if clear == 0:
            return (0, 0, 0)
        red = int(pow((int((r / clear) * 256) / 255), 2.5) * 255)
        green = int(pow((int((g / clear) * 256) / 255), 2.5) * 255)
        blue = int(pow((int((b / clear) * 256) / 255), 2.5) * 255)
        return (min(255, red), min(255, green), min(255, blue))

    def read_rgb(self):
        return self.color_rgb_bytes(self.sensor.read(True))

    def calculate_absorbance(self, transmitted_light):
        if self.incident_light is None:
            raise ValueError(
                "Incident light intensity must be set before calculating absorbance."
            )
        if transmitted_light <= 0:
            raise ValueError(
                "Transmitted light must be greater than 0 to calculate absorbance."
            )
        transmittance = transmitted_light / self.incident_light
        absorbance = -math.log10(transmittance)
        return absorbance

    def display_on_oled(self, r, g, b, absorbance):
        self.oled.fill(0)
        self.oled.text("R: {}".format(r), 0, 8)
        self.oled.text("G: {}".format(g), 0, 16)
        self.oled.text("B: {}".format(b), 0, 24)
        self.oled.text("Abs: {:.3f}".format(absorbance), 0, 32)
        self.oled.show()

    def log_and_publish_absorbance(self, absorbance, mqtt_connection, wifi_connection):
        print(f"Absorbance: {absorbance:.3f}")
        log_entry = f"{time.time()}, {absorbance:.3f}\n"
        with open("absorbance_log.txt", "a") as log_file:
            log_file.write(log_entry)

        if wifi_connection.is_connected():
            try:
                mqtt_connection.publish("design4.absorbance", str(absorbance))
                print("Absorbance data published.")
            except Exception as e:
                print(f"Failed to publish absorbance data: {type(e).__name__} {e}")
        else:
            print("WiFi is disconnected. Storing data locally.")

    def run(self, mqtt_connection, wifi_connection):
        while True:
            r, g, b = self.read_rgb()
            transmitted_light = max(
                r, g, b
            )  # Using the maximum of RGB as transmitted light for example
            absorbance = self.calculate_absorbance(transmitted_light)
            self.display_on_oled(r, g, b, absorbance)
            self.log_and_publish_absorbance(
                absorbance, mqtt_connection, wifi_connection
            )
            time.sleep(1)


class Client:
    def __init__(
        self,
        aio_username: bytes,
        aio_key: bytes,
        ssid: str,
        ssid_pw: str,
        mqtt_client_id: bytes,
    ) -> None:
        self.AIO_USERNAME = aio_username
        self.AIO_KEY = aio_key
        self.client = None
        self.broker = b"io.adafruit.com"
        self.port = 1883
        self.client_id = mqtt_client_id
        self.SSID = ssid
        self.PW = ssid_pw
        self.pid = PID.PID(1.0, 0.1, 0.01, 25.0)
        self.rgb_sensor = ColorSensor()

    def connect_to_wifi(self):
        ap_if = network.WLAN(network.AP_IF)
        ap_if.active(False)

        wifi = network.WLAN(network.STA_IF)
        wifi.active(True)
        wifi.connect(self.SSID, self.PW)

        MAX_ATTEMPTS = 20
        attempt_count = 0

        while not wifi.isconnected() and attempt_count < MAX_ATTEMPTS:
            attempt_count += 1
            time.sleep(1)

        if attempt_count == MAX_ATTEMPTS:
            print("Could not connect to the WiFi network")
            sys.exit()
        print("Connected to WiFi")

    def connect(self):
        self.connect_to_wifi()

        self.client = MQTTClient(
            client_id=self.client_id,
            server=self.broker,
            user=self.AIO_USERNAME,
            password=self.AIO_KEY,
            ssl=False,
        )

        try:
            self.client.connect()
            print("Connected to Adafruit IO")
        except Exception as e:
            print("Could not connect to MQTT server {}{}".format(type(e).__name__, e))
            sys.exit()

    def disconnect(self):
        if self.client:
            self.client.disconnect()
            print("Disconnected from Adafruit IO")

    def get_feedname(self, feedname: bytes):
        mqtt_feedname = bytes(
            "{:s}/feeds/{:s}".format(
                self.AIO_USERNAME.decode("utf-8"), feedname.decode("utf-8")
            ),
            "utf-8",
        )
        return mqtt_feedname

    def publish(self, feedname: bytes, message: str):
        if not self.client:
            print("Not connected to MQTT broker")
            return
        feed_url = self.get_feedname(feedname)
        self.client.publish(feed_url, message)
        print(f"Published {message} to {feed_url}")

    def publish_periodic(self, message_func, feedname: bytes, publish_period_sec=10):
        accum_time = 0
        try:
            while True:
                if accum_time >= publish_period_sec:
                    message = message_func()
                    self.publish(feedname, message)
                    accum_time = 0
                time.sleep(0.5)
                accum_time += 0.5
        except KeyboardInterrupt:
            print("Ctrl-C pressed...exiting")
            self.disconnect()
            sys.exit()

    def subscribe(self, feedname: bytes):
        if not self.client:
            print("Not connected to MQTT broker")
            return
        self.client.set_callback(self.on_message)
        feed_url = self.get_feedname(feedname)
        self.client.subscribe(feed_url)
        print(f"Subscribed to {feed_url}")

    def subscribe_and_check(self, feedname: bytes, check_period_sec=0.5):
        self.subscribe(feedname)
        try:
            while True:
                self.client.check_msg()
                time.sleep(check_period_sec)
        except KeyboardInterrupt:
            print("Ctrl-C pressed...exiting")
            self.disconnect()
            sys.exit()

    def on_message(self, topic, msg):
        print(f"Message received: {msg} on topic: {topic}")
        if b"pid.proportional" in topic:
            self.handle_proportional(msg)
        elif b"pid.integral" in topic:
            self.handle_integral(msg)
        elif b"pid.derivative" in topic:
            self.handle_derivative(msg)
        elif b"led.control" in topic:
            self.handle_led(msg)

    def handle_proportional(self, msg: bytes) -> None:
        try:
            value = float(msg.decode("utf-8"))
            self.pid.set_Kp(value)
            print(f"Proportional value updated to: {value}")
        except ValueError:
            print(f"Invalid proportional value: {msg}")

    def handle_integral(self, msg: bytes) -> None:
        try:
            value = float(msg.decode("utf-8"))
            self.pid.set_Ki(value)
            print(f"Integral value updated to: {value}")
        except ValueError:
            print(f"Invalid integral value: {msg}")

    def handle_derivative(self, msg: bytes) -> None:
        try:
            value = float(msg.decode("utf-8"))
            self.pid.set_Kd(value)
            print(f"Derivative value updated to: {value}")
        except ValueError:
            print(f"Invalid derivative value: {msg}")

    def handle_temperature(self, msg: bytes) -> None:
        print(f"Temperature value received: {msg}")

    def handle_led(self, msg: bytes) -> None:
        print(f"LED control message received: {msg}")

    def check_messages(self):
        if not self.client:
            print("Not connected to MQTT broker")
            return
        self.client.check_msg()

    def fetch_all_feeds(self):
        url = (
            f"https://io.adafruit.com/api/v2/{self.AIO_USERNAME.decode('utf-8')}/feeds"
        )
        headers = {"X-AIO-Key": self.AIO_KEY.decode("utf-8")}
        response = requests.get(url, headers=headers)

        if response.status_code == 200:
            return response.json()
        else:
            print(f"Failed to fetch feeds: {response.status_code}")
            return None

    def fetch_feed_data(self, feed_key: str, start_time: str, end_time: str):
        url = f"https://io.adafruit.com/api/v2/{self.AIO_USERNAME.decode('utf-8')}/feeds/{feed_key}/data"
        headers = {"X-AIO-Key": self.AIO_KEY.decode("utf-8")}
        params = {"start_time": start_time, "end_time": end_time}
        response = requests.get(url, headers=headers, params=params)

        if response.status_code == 200:
            return response.json()
        else:
            print(f"Failed to fetch data for feed {feed_key}: {response.status_code}")
            return None

    def write_to_json(self, data, filename="feed_data.json"):
        with open(filename, "w") as file:
            json.dump(data, file, indent=4)
        print(f"Data written to {filename}")

    def temperature_thread(self):
        temperature = TemperatureSensor(32)
        self.publish_periodic(
            temperature.read_temperature,
            feedname=b"temperature",
            publish_period_sec=10,
        )

    def rgb_thread(self):
        self.rgb_sensor.run(self, self)

    def control_thread(self):
        try:
            while True:
                self.client.check_msg()
                time.sleep(0.5)
        except KeyboardInterrupt:
            print("Ctrl-C pressed...exiting")
            self.disconnect()
            sys.exit()


class TemperatureSensor:
    NOM_RES = 10000
    SER_RES = 9820
    TEMP_NOM = 25
    NUM_SAMPLES = 25
    THERM_B_COEFF = 3950
    ADC_MAX = 1023
    ADC_Vmax = 3.15

    def __init__(self, TEMP_SENS_ADC_PIN_NO=32):
        self.adc = self.init_temp_sensor(TEMP_SENS_ADC_PIN_NO)

    def init_temp_sensor(self, TEMP_SENS_ADC_PIN_NO):
        adc = ADC(Pin(TEMP_SENS_ADC_PIN_NO))
        adc.atten(ADC.ATTN_11DB)
        adc.width(ADC.WIDTH_10BIT)
        return adc

    def read_temperature(self):
        raw_read = []
        for i in range(self.NUM_SAMPLES):
            raw_read.append(self.adc.read())

        raw_average = sum(raw_read) / self.NUM_SAMPLES
        print("raw_avg = " + str(raw_average))
        adc_voltage = self.adc.read_u16() * (self.ADC_Vmax / 65535.0)
        print("V_measured = " + str(adc_voltage))

        raw_average = self.ADC_MAX * adc_voltage / self.ADC_Vmax
        resistance = (self.SER_RES * raw_average) / (self.ADC_MAX - raw_average)
        print("Thermistor resistance: {} ohms".format(resistance))

        steinhart = math.log(resistance / self.NOM_RES) / self.THERM_B_COEFF
        steinhart += 1.0 / (self.TEMP_NOM + 273.15)
        steinhart = (1.0 / steinhart) - 273.15
        return steinhart


class DC_MOTOR:
    frequency = 1000
    currentDutyCycle = 0
    maxCycles = 1023
    minCycles = 123

    def __init__(self, inputD, inputC, EnableB) -> None:
        self.inputD = Pin(inputD, Pin.OUT)
        self.inputC = Pin(inputC, Pin.OUT)
        self.EnableB = Pin(EnableB, Pin.OUT)
        self.pwmB = machine.PWM(self.EnableB)
        self.pwmB.freq(self.frequency)

    def start(self):
        self.inputD.value(1)
        self.inputC.value(0)

    def stop(self):
        self.inputD.value(0)
        self.inputC.value(0)

    def setSpeedCycles(self, dutyCycles):
        self.currentDutyCycle = dutyCycles
        self.pwmB.duty(dutyCycles)

    def setSpeedPercentage(self, percentage):
        self.currentDutyCycle = self.percentageToDutyCycle(percentage)
        self.pwmB.duty(int(self.currentDutyCycle))

    def getSpeedPercentage(self):
        percentage = self.dutyCycleToPercentage(self.currentDutyCycle)
        return percentage

    def dutyCycleToPercentage(self, dutyCycle):
        percentage = 100 * (
            (dutyCycle - self.minCycles) / (self.maxCycles - self.minCycles)
        )
        return percentage

    def percentageToDutyCycle(self, percentage):
        dutyCycle = (
            (self.maxCycles - self.minCycles) * percentage / 100
        ) + self.minCycles
        return dutyCycle

    def testMinAndMaxDuty(self):
        print("running large DC testMinAndMaxDuty....")

        self.start()
        duty = 0
        while duty < self.maxCycles:
            self.setSpeedCycles(duty)
            time.sleep(0.01)
            duty += 1

        print("Test is done")
        time.sleep(3)
        self.stop()

    def testMaxSpeed(self):
        print("running large DC testMaxSpeed....")
        self.start()
        for _ in range(1000):
            print("running at max speed")
            self.setSpeedCycles(self.maxCycles)
            time.sleep(0.01)

        print("Test is done")
        self.stop()


if __name__ == "__main__":
    aio_username = b"ajcs"
    aio_key = b"aio_tmLB64jFfELSf9wXUJNdCXsmZeve"
    ssid = "Arturowifi"
    ssid_pw = "ambautukan"
    mqtt_client_id = bytes(
        "client_" + str(int.from_bytes(os.urandom(3), "little")), "utf-8"
    )

    mqtt_client = Client(aio_username, aio_key, ssid, ssid_pw, mqtt_client_id)
    mqtt_client.connect()

    def get_free_heap():
        return str(gc.mem_free())

    from datetime import datetime, timedelta

    end_time = datetime.utcnow().isoformat()
    start_time = (datetime.utcnow() - timedelta(days=7)).isoformat()

    feeds = mqtt_client.fetch_all_feeds()
    if feeds:
        all_data = {}
        for feed in feeds:
            feed_key = feed["key"]
            data = mqtt_client.fetch_feed_data(feed_key, start_time, end_time)
            if data:
                all_data[feed_key] = data

        mqtt_client.write_to_json(all_data, filename="all_feeds_data.json")

    try:
        temperature_thread = threading.Thread(target=mqtt_client.temperature_thread)
        temperature_thread.start()

        rgb_thread = threading.Thread(target=mqtt_client.rgb_thread)
        rgb_thread.start()

        control_thread = threading.Thread(target=mqtt_client.control_thread)
        control_thread.start()

        temperature_thread.join()
        rgb_thread.join()
        control_thread.join()
    except KeyboardInterrupt:
        print("Interrupted")
    finally:
        mqtt_client.disconnect()
