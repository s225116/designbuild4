double errSum = 0.0;
double errPrev = 0.0;
double sum = 0.0;

double getPID(double actualT, double desiredT){
    double P, I, D; // figure out values needed for ideal PID controller
    double et = desiredT - actualT; // calculate error

    // apply PID formula
    double ut = P*et+ I*(sum+et) + D * (et -errPrev);


    errPrev = et;
    sum += et;

    //turn ut into amount of seconds we need to pump or volume of water
    //if needed turn on the fan
    return ut;
}


