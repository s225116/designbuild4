from machine import Pin
import utime


class stepMotor:
    def __init__(self, pinDirection, pinStep) -> None:
        self.direction = Pin(pinDirection, Pin.OUT)
        self.step = Pin(pinStep, Pin.OUT)
        self.speed = 1

    def oneStep(self):
        self.step.value(1 - self.step.value())

    def dir_clockwise(self):
        self.direction.value(1)

    def dir_cclockwise(self):
        self.direction.value(0)

    def set_speed(self, val):
        self.speed = val

    def cycle(self, stepsToPerform):
        # 3200 steps equals to one full rotation
        for i in range(stepsToPerform):
            self.oneStep()
            utime.sleep_ms(2)
            self.oneStep() 
            utime.sleep_ms(2)

    def intermittent_step(self, sleep):
        self.oneStep()
        utime.sleep_ms(sleep)
        self.oneStep()
        utime.sleep_ms(sleep)

    def cycle_speed(self):
        self.intermittent_step(self.speed)        


    def run(self, time):
        i = 0
        while(i < time*1000):
            self.cycle_speed()
            i = i + 1
         
#Step pin A
#A = Pin(26, Pin.OUT)
#Dir pin A1
#A = Pin(25, Pin.OUT)
step = stepMotor(25, 26)
step.dir_clockwise()
step.set_speed(2)from machine import Pin
import utime


class PumpStep:
    def _init_(self, pinDirection, pinStep) -> None:
        self.direction = Pin(pinDirection, Pin.OUT)
        self.step = Pin(pinStep, Pin.OUT)

    def oneStep(self):
        self.step.value(1 - self.step.value())

    def direction_clockwise(self):
        self.direction.value(1)

    def direction_counterclockwise(self):
        self.direction.value(0)

    def cycle(self, stepsToPerform):
        # 3200 steps equals to one full rotation
        for i in range(stepsToPerform):
            self.oneStep()
            utime.sleep_us(10)
            self.oneStep()
            utime.sleep_us(10)
        # print(i)

    def intermittent_step(self, sleep):
        self.oneStep()
        utime.sleep_us(sleep)
        self.oneStep()
        utime.sleep_us(sleep)


#Step pin
#A4 = Pin(36, Pin.OUT)
#Dir pin
#A5 = Pin(4, Pin.OUT)
step = PumpStep(36, 4)

step.direction_clockwise()
step.cycle(1000)


